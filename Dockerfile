FROM openjdk:8-alpine

LABEL name="Carlos Garcia Gonzalez" maintainer="cgarcia.gonzalez@atsistemas.com"

RUN mkdir /opt \
&& apk update \
&& apk add curl \
&& apk add unzip 

# Install liferay 
RUN curl -O -s -k -L -C - http://downloads.sourceforge.net/project/lportal/Liferay%20Portal/6.2.5%20GA6/liferay-portal-tomcat-6.2-ce-ga6-20160112152609836.zip \
&& unzip liferay-portal-tomcat-6.2-ce-ga6-20160112152609836.zip -d /opt \
&& rm liferay-portal-tomcat-6.2-ce-ga6-20160112152609836.zip